﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace RecipeBuilder.Web.Api.Extensions
{
    public static class ContentExtensions
    {
        public static void ExecuteIfPropertyExists(this IContent content, string property, Action<object> propAction)
        {
            if (content.Properties.Contains(property)
                && content.Properties[property] != null &&
                content.Properties[property].Value != null)
            {
                propAction(content.Properties[property].Value);
            }
        }
    }
}
