﻿using RecipeBuilder.Web.Api.Factories;
using RecipeBuilder.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core;

namespace RecipeBuilder.Web.Api.Controllers
{
    [RoutePrefix("api/Recipe")]
    public class RecipeController : ApiController
    {
        private RecipeFactory recipeFactory;

        public RecipeController()
        {
            this.recipeFactory = new RecipeFactory(ApplicationContext.Current.Services.ContentService);
        }

        [HttpGet]
        [Route]
        public List<Recipe> GetAll()
        {
            return this.recipeFactory.GetAllRecipes();
        }

        [HttpPost]
        [Route]
        public Recipe AddRecipe(AddRecipeRequest request)
        {
            Recipe newRecipe = this.recipeFactory.CreateNewRecipe();

            newRecipe.Name = request.Name;
            newRecipe.Style = request.Style;
            newRecipe.AlcoholByVolume = request.Abv;
            newRecipe.Description = request.Description;
            newRecipe.VolumeInGallons = request.Gallons;

            newRecipe.Save();

            return newRecipe;
        }

        [HttpPut]
        [Route("{recipeId}")]
        public Recipe UpdateRecipe(int recipeId, UpdateRecipeRequest request)
        {
            Recipe foundRecipe = this.recipeFactory.GetRecipe(recipeId);

            request.Properties.ForEach((prop) =>
                {
                    if(prop.Alias == "name")
                    {
                        foundRecipe.Name = prop.Value;
                    }
                    if(prop.Alias == "style")
                    {
                        foundRecipe.Style = prop.Value;
                    }
                });

            foundRecipe.Save();

            return foundRecipe;
        }
    }
}
