﻿using System.Collections.Generic;
using System.Web.Http;

namespace RecipeBuilder.Web.Api.Controllers
{
    [RoutePrefix("api/Values")]
    public class ValuesController : ApiController
    {
        [Route]
        public List<string> GetValues()
        {
            return new List<string>()
            {
                "value one",
                "value two"
            };
        }
    }
}
