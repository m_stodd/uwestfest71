﻿using RecipeBuilder.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace RecipeBuilder.Web.Api.Factories
{
    public class RecipeFactory
    {
        private IContentService contentService;

        public RecipeFactory(IContentService contentService)
        {
            this.contentService = contentService;
        }

        public Recipe CreateNewRecipe()
        {
            return new Recipe(this.contentService);
        }

        public List<Recipe> GetAllRecipes()
        {
            List<Recipe> allRecipes = new List<Recipe>();

            this.contentService.GetContentOfContentType(Recipe.CONTENT_TYPE_ID).ToList().ForEach((content) =>
            {
                Recipe recipe = new Recipe(this.contentService, content);
                allRecipes.Add(recipe);
            });

            return allRecipes;
        }

        public  Recipe GetRecipe(int recipeId)
        {
            IContent foundContent = this.contentService.GetById(recipeId);

            Recipe foundRecipe = new Recipe(this.contentService, foundContent);

            return foundRecipe;
        }
    }
}
