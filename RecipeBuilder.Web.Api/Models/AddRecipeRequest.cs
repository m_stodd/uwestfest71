﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBuilder.Web.Api.Models
{
    public class AddRecipeRequest
    {
        public string Name { get; set; }

        public string Style { get; set; }

        public int Gallons { get; set; }

        public float Abv { get; set; }

        public string Description { get; set; }
    }
}
