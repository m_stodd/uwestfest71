﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using RecipeBuilder.Web.Api.Extensions;

namespace RecipeBuilder.Web.Api.Models
{
    public class Recipe
    {
        private static int RECIPES_NODE = 1081;
        public static int CONTENT_TYPE_ID = 1080;

        private IContentService contentService;
        private IContent source;

        public Recipe(IContentService contentService)
        {
            this.contentService = contentService;
        }

        public Recipe(IContentService contentService, IContent source)
        {
            this.contentService = contentService;
            this.source = source;
            this.Load();
        }

        private void Load()
        {
            this.source.ExecuteIfPropertyExists("name", prop =>
            {
                this.Name = prop.ToString();
            });

            this.source.ExecuteIfPropertyExists("volumeInGallons", prop =>
            {
                int volume;
                if (Int32.TryParse(prop.ToString(), out volume))
                {
                    this.VolumeInGallons = volume;
                }
            });

            this.source.ExecuteIfPropertyExists("alcoholByVolume", prop =>
            {
                float abv;
                if (float.TryParse(prop.ToString(), out abv))
                {
                    this.AlcoholByVolume = abv;
                }
            });

            this.source.ExecuteIfPropertyExists("style", prop =>
            {
                this.Style = prop.ToString();
            });

            this.source.ExecuteIfPropertyExists("description", prop =>
            {
                this.Description = prop.ToString();
            });

            this.RecipeId = this.source.Id;
        }
        
        public void Save()
        {
            if(this.source == null)
            {
                IContent umbracoRecipe = this.contentService.CreateContent(this.Name, RECIPES_NODE, "Recipe");
                this.source = umbracoRecipe;
            }

            this.source.SetValue("name", this.Name);
            this.source.SetValue("style", this.Style);
            this.source.SetValue("volumeInGallons", this.VolumeInGallons);
            this.source.SetValue("alcoholByVolume", this.AlcoholByVolume.ToString());
            this.source.SetValue("description", this.Description);

            this.contentService.SaveAndPublishWithStatus(this.source);
        }

        public int RecipeId { get; set; }

        public string Name { get; set; }

        public int VolumeInGallons { get; set; }

        public float AlcoholByVolume { get; set; }

        public string Style { get; set; }

        public string Description { get; set; }
    }
}
