﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBuilder.Web.Api.Models
{
    public class Property
    {
        public string Alias { get; set; }

        public string Value { get; set; }
    }
}
