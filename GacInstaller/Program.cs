﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GacInstaller
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Run();
            System.Console.WriteLine("If ran AS ADMINISTRATOR, your assemblies are now installed.");
            System.Console.ReadLine();
        }

        public void Run()
        {
            this.InstallToGac();
        }

        private void InstallToGac()
        {
            string path = Directory.GetCurrentDirectory();

            Publish pub = new Publish();
            System.Console.WriteLine("Installing System.Web.Http.dll to the GAC");
            pub.GacInstall(Path.Combine(path, "System.Web.Http.dll"));

            System.Console.WriteLine("Installing System.Net.Http.Formatting.dll to the GAC");
            pub.GacInstall(Path.Combine(path, "System.Net.Http.Formatting.dll"));

            System.Console.WriteLine("Installing System.Web.Http.WebHost.dll to the GAC");
            pub.GacInstall(Path.Combine(path, "System.Web.Http.WebHost.dll"));
        }
    }
}
