﻿function RecipeClient(baseUrl) {
    this.baseUrl = baseUrl;
}

RecipeClient.prototype.GetAll = function () {
    var recipes;

    $.ajax({
        async: false,
        type: 'GET',
        url: this.baseUrl + '/api/Recipe',
        success: function (data, status, xhr) {
            recipes = data;
        }
    });

    return recipes
}

RecipeClient.prototype.AddRecipe = function (name, volume, abv, style, recipe) {
    var newRecipe;

    $.ajax({
        async: false,
        type: 'POST',
        url: this.baseUrl + '/api/Recipe',
        dataType: "json",
        data: {
            Name: name,
            Gallons: volume,
            Abv: abv,
            Style: style,
            Description: recipe
        },
        success: function (data, status, xhr) {
            newRecipe = data;
        }
    });
    return newRecipe;
}

RecipeClient.prototype.Update = function (recipeId, alias, value) {
    var updatedRecipe;

    $.ajax({
        async: false,
        type: 'PUT',
        url: this.baseUrl + '/api/Recipe/' + recipeId,
        data: {
            Properties: [
                {
                    Alias: alias,
                    Value: value
                }]
        },
        success: function (data, status, xhr) {
            updatedRecipe = data;
        }
    });

    return updatedRecipe;
}