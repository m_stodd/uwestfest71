﻿angular.module("recipeBuilderApp").controller("recipeController", function ($scope, $http) {

    $scope.recipes = recipeClient.GetAll();

    $scope.AddRecipe = function () {
        var name = $scope.newRecipe.name;
        var style = $scope.newRecipe.style;
        var abv = Number($scope.newRecipe.abv);
        var gallons = Number($scope.newRecipe.gallons);
        var description = $scope.newRecipe.description;

        newRecipe = recipeClient.AddRecipe(name, gallons, abv, style, description);
        $scope.recipes.push(newRecipe);
    }

    $scope.UpdateRecipe = function (recipeId, alias, value) {
        var updatedRecipe = recipeClient.Update(recipeId, alias, value);

        for (var i = 0; i < $scope.recipes.length; i++) {
            if($scope.recipes[i].RecipeId == updatedRecipe.RecipeId){
                $scope.recipes[i] = updatedRecipe;
                break;
            }
        }
    }

    $scope.newRecipe = 
        {
            name: '',
            abv: 0,
            gallons: 0,
            style: '',
            description: ''
        }
});
